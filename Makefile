include /usr/share/dpkg/pkg-info.mk

PACKAGE=proxmox-mailgateway

BUILDDIR=$(PACKAGE)-$(DEB_VERSION)
DSC=$(PACKAGE)_$(DEB_VERSION).dsc

DEB=$(PACKAGE)_$(DEB_VERSION_UPSTREAM_REVISION)_all.deb
CT_DEB=$(PACKAGE)-container_$(DEB_VERSION_UPSTREAM_REVISION)_all.deb
HEADER_DEB=pve-headers_$(DEB_VERSION_UPSTREAM_REVISION)_all.deb

DEBS=$(DEB) $(CT_DEB) $(HEADER_DEB)

all: $(DEB)

.PHONY: deb
deb $(CT_DEB) $(HEADER_DEB): $(DEB)

$(BUILDDIR): debian
	rm -rf $@ $@.tmp
	mkdir -p $@.tmp/debian
	cp -a debian/ $@.tmp/
	echo "git clone git://git.proxmox.com/git/proxmox-mailgateway.git\\ngit checkout $(shell git rev-parse HEAD)" > $@.tmp/debian/SOURCE
	mv $@.tmp $@

$(DEB): $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -b -uc -us
	lintian $(DEBS)

$(DSC): $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -S -uc -us

.PHONY:dsc
dsc: $(DSC)
	$(MAKE) clean
	$(MAKE) $(DSC)
	lintian $(DSC)

.PHONY: sbuild
sbuild: $(DSC)
	sbuild $(DSC)

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(DEBS)
	tar cf - $(DEBS)|ssh -X repoman@repo.proxmox.com -- upload --product pmg --dist $(UPLOAD_DIST)

clean:
	rm -rf $(PACKAGE)-[0-9]*/
	rm -f $(PACKAGE)*.tar.* *.deb *.dsc *.build *.buildinfo *.changes
